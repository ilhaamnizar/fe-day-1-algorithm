function checkPrimeNumber(n) {
    if (n===1) {
      return ('Bukan bilangan prima');
    } else if (n === 2) {
      return ('Bilangan prima');
    } else {
      for(var i = 2; i < n; i++) {
        if(n % i === 0) {
          return ('Bukan bilangan prima');
        }
      }
      return('Bilangan Prima');  
    }
}
  
  console.log(checkPrimeNumber(2));